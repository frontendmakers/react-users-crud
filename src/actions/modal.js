import * as HttpRequests from '../services/httpRequests';

export const showUserEditModal = () => {
  return dispatch => {
    // console.log('SHOW_USER_EDIT_MODAL');
    dispatch({ type: 'SET_DISPLAY_MODAL', payload: true});
    dispatch({ type: 'SET_MODAL_CONTENT', payload: { userEdit: true } });
  }
}


export const showUserCreateModal = () => {
  return dispatch => {
    // console.log('SHOW_USER_EDIT_MODAL');
    dispatch({ type: 'SET_DISPLAY_MODAL', payload: true});
    dispatch({ type: 'SET_MODAL_CONTENT', payload: { userCreate: true } });
  }
}

export const showAlert = (message) => {
  return dispatch => {
    dispatch( { type: 'SET_ALERTED', payload: { message: message, visible: true } });
    setTimeout(() => {
      dispatch({ type: 'SET_ALERTED', payload: { message: '', visible: false } });
    }, 2000);
  }
}