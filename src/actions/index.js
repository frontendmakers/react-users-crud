import * as HttpRequests from '../services/httpRequests';
import * as ModalActions from './modal';

const NUMBER_USERS_IN_PAGE = 6;

const ERRORS = {
    LOAD_USERS: 'Can not load Users. Please, check your connection',
    LOAD_USER: 'Can not load User. Please, check your connection',
    ADD_USER: 'Can not add User. Please, check your connection',
    UPDATE_USER: 'Can not update User. Please check your connection',
    DELETE_USER: 'Can not delete User. Please check your connection'
};

export const getNumberUsers = (search) => {
    return dispatch => {
        HttpRequests.getNumberUsers(search).then(
            result => {
                let numberUsers = result.length
                dispatch({ type: 'SET_NUMBER_USERS', payload: numberUsers });
                let numberPages = Math.ceil(numberUsers / NUMBER_USERS_IN_PAGE);
                dispatch({ type: 'SET_NUMBER_PAGES', payload: numberPages });
            },
            error => {
                console.log(error);
            }
        )
    }
}

export const getUsers = (page, search) => {
  return dispatch => {
      dispatch({ type: 'CLEAR_SELECTED' });
      dispatch({ type: 'SET_IS_LOADING', payload: { users: true }});
      console.log('getuSERS');
      if (!page) page = 1;
      HttpRequests.searchUsers(page, NUMBER_USERS_IN_PAGE, search).then(
            result => {
              console.log(result);
              dispatch({ type: 'SET_IS_LOADING', payload: { users: false }});
              if ( result.length === 0 ) {
                dispatch({ type: 'SET_ERROR', payload: { users: 'Users not found.' } });
                return;
              }
              dispatch({ type: 'CLEAR_ERROR_USERS'});
              dispatch({ type: 'SET_USERS', payload: result });
          },
          error => {
              console.log(error.message);
              let err = ( error.message === 'Failed to fetch') ? ERRORS.LOAD_USERS : error.message;
              dispatch({ type: 'SET_ERROR', payload: { users: err } });
          }
      );
  }
};

export const getUser = (id, users) => {
    return dispatch => {
        dispatch(ModalActions.showUserEditModal());
        dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: true } });
        let user = users.find( value => value.id == id);
        if (user) {
            dispatch({ type: 'SET_USER', payload: user });
            dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: false } });
            dispatch( { type: 'CLEAR_ERROR_USER' });

            return;
        }
        HttpRequests.getUser(id).then(
            result => {
                user = result;
                dispatch({ type: 'SET_USER', payload: user });
                dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: false } });
                dispatch( { type: 'CLEAR_ERROR_USER' });

            },
            error => {
                let err = ( error.message === 'Failed to fetch') ? ERRORS.LOAD_USER : error.message;
                dispatch( { type: 'SET_ERROR', payload: { user: err } });
                
                dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: false } });
                
            }
        )
    }
}

export const deleteUser = (id) => {
    return dispatch => {
        // console.log('DELETE_USER_ACTION. ID: ' + id);
        dispatch({ type: 'PUSH_TO_USERS_TO_DELETE', payload: id });
        dispatch({ type: 'CLEAR_SELECTED' });
        HttpRequests.deleteUser(id).then(
            result => {
                dispatch({ type: 'DELETE_USER', id: id });
                dispatch({ type: 'DEC_NUMBER_USERS', payload: NUMBER_USERS_IN_PAGE});
                dispatch({ type: 'DELETE_FROM_USERS_TO_DELETE', payload: id });

            },
            error => {
                let err = ( error.message === 'Failed to fetch') ? ERRORS.DELETE_USER : error.message;
                dispatch(ModalActions.showAlert(err));
                dispatch({ type: 'DELETE_FROM_USERS_TO_DELETE', payload: id });

            });
    }
}

export const deleteUsers = (toDelete) => {
    return dispatch => {
        // dispatch({ type: 'SET_USERS_TO_DELETE', payload: toDelete });
        dispatch({ type: 'CLEAR_SELECTED' });
        toDelete.forEach(element => {
            dispatch(deleteUser(element));
        });
    }
} 

export const addUser = (user) => {
    return dispatch => {
        // console.log('ADD_USER_ACTION');
        dispatch({ type: 'SET_IS_LOADING', payload: { userCreate: true } });
        return new Promise((resolve, reject) => {
            HttpRequests.addUser(user).then(
                result => {
                    dispatch({ type: 'ADD_USER', payload: result});
                    dispatch({ type: 'INC_NUMBER_USERS', payload: NUMBER_USERS_IN_PAGE});
                    dispatch({ type: 'CLOSE_MODAL' });
                    dispatch({ type: 'SET_IS_LOADING', payload: { userCreate: false } });
                    // dispatch({ type: 'SET_CURRENT_PAGE', payload: 1});
                    resolve(result);
                },
                error => {
                    dispatch({ type: 'SET_IS_LOADING', payload: { userCreate: false } });
                    let err = ( error.message === 'Failed to fetch') ? ERRORS.ADD_USER : error.message;
                    dispatch(ModalActions.showAlert(err));
                    reject(error);
                }
            );

        });
    }
}

export const updateUser = (user) => {
    return dispatch => {
        // console.log('UPDATE_USER_ACTION');
        dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: true } });
        return new Promise((resolve, reject) => {
            HttpRequests.updateUser(user).then(
                result => {
                    dispatch({ type: 'SET_USER_TO_USERS', payload: result });
                    dispatch({ type: 'CLOSE_MODAL' });
                    dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: false } });
                    resolve(result);
                },
                error => {
                    dispatch({ type: 'SET_IS_LOADING', payload: { userEdit: false } });
                    let err = ( error.message === 'Failed to fetch') ? ERRORS.UPDATE_USER : error.message;
                    dispatch(ModalActions.showAlert(err));
                    reject(error.message);
                }
            );
        });
    }
}

export const getCities = () => {
    return dispatch => {
        HttpRequests.getCities().then(
            result => {
                dispatch( { type: 'SET_CITIES', payload: result });
            },
            error => {
                // console.log(error.message);
                console.log('Can not get cities');
            }
        )
    }
}


