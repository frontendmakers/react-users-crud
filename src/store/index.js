import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
// store.subscribe(() => {
//     console.log('subscribe', store.getState());
// })

// store.dispatch({ type: 'ADD_USER', user: 'From Smile everybody Will Happy'});
