import React from 'react';
import UserCreate from './UserCreate';
import UserEdit from './UserEdit';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import querystring from 'query-string';
import Preloader from './Preloader';
import { TransitionMotion, spring } from 'react-motion';

const willEnter = () => ({ opacity: 0, x: -2000 });
const willLeave = () => ({ 
    opacity: spring(0,{stiffness: 150, damping: 20}), 
    x: spring(2000, {stiffness: 110, damping: 20, precision: 200})
 });

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            errorsForm: []
        });
    }

    render() {
        return (
            <div>
                { 
                    <TransitionMotion 
                        willEnter={ willEnter }
                        willLeave={ willLeave }
                        styles={
                            this.props.displayModal ? 
                            [{ key: 'modal', data: {}, style: { 
                                opacity: spring(1),
                                x: spring(0, { stiffness: 100, damping: 10, precision: 10 })                           
                            }}] 
                            :
                            []
                        }
                    >
                        { (items) => (
                            <div>
                                {items.map(item => {
                                    return (
                                        <div className="modal-backdrop" 
                                            key={ item.key }
                                            style={ { opacity: item.style.opacity } }
                                        >
                                            {item.style.opacity}<br/>
                                            {item.style.x}
                                            <div className="row">
                                                <div className="col s12 m8 offset-m2 " >
                                                    <div className="modal green lighten-3" 
                                                        style={ { 
                                                            transform: `translateX(${ item.style.x }px) translateY(-50%)`,
                                                            opacity: item.style.opacity ,
                                                            transform: `translate(${ item.style.x }px, -50%)`
                                                        } } 
                                                    >
                                                        <span onClick={() => this.closeModal()} className="modal-close">x</span>
                                                        {
                                                            this.props.error.length > 0 ?
                                                            <div className="error">
                                                            { this.props.error }
                                                        </div>
                                                        :
                                                        <div className="success">
                                                            { this.props.isLoading && <Preloader></Preloader> }
                                                            { this.props.modalContent.userCreate && 
                                                            <UserCreate 
                                                            closeModal={() => this.closeModal()} 
                                                                addUser={ (user) =>this.addUser(user) } 
                                                                isLoading={this.props.isLoading}
                                                                cities={this.props.cities}
                                                                >
                                                            </UserCreate> }
                                                            { this.props.modalContent.userEdit && 
                                                            <UserEdit
                                                                error={ this.props.error }
                                                                closeModal={ () => this.closeModal() } 
                                                                user={ this.props.user } 
                                                                updateUser={(user) => this.updateUser(user)}
                                                                isLoading={this.props.isLoading}
                                                                cities={this.props.cities}
                                                            >
                                                            </UserEdit> }
                                                            <div className="errors-form">
                                                                {this.state.errorsForm.map((element, index) => 
                                                                <span key={index}>{element}  </span>)}
                                                            </div>
                                                        </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    );
                                })}
                                

                            </div>
                        ) }
                    </TransitionMotion>

                }
                </div>
        );
    }

    addUser(user) {
        if (this.checkUser(user)) {
            this.props.addUser(user).then(
                result => {
                    this.closeModal();
                },
                error => {
                    console.log(error);
                }
            );
        }
    }

    updateUser(user) {
        if (this.checkUser(user)){
            this.props.updateUser(user).then(
                result => {
                    this.closeModal();
                },
                error => {
                    console.log(error);
                    this.props.setUser(user);
                }
            );
        } else {
            this.props.setUser(user);
        }
    }
    closeModal() {
        this.props.closeModal();
        let searchObj = querystring.parse(this.props.location.search);
        delete searchObj.edit;
        if (searchObj.create) {
            this.props.history.push('/users');
            return;
        }
        this.setState({ errorsForm: [] });
        // delete searchObj.create;
        let search = querystring.stringify(searchObj);
        this.props.history.push(`/users?${search}`);
    }

    checkUser(user) {
        let errors = [];
        if (!user.name) errors.push('Enter user name!');
        if (!user.city) errors.push('Enter city!');
        if (!user.email) {
            errors.push('Enter email!');
        } else {
            let reg = /^[A-Za-z0-9](\.?[A-Za-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/g;
            if (!reg.test(user.email)) {
                errors.push('Enter correct email!');
            }
        }
        this.setState({ errorsForm: [].concat(errors) });
        return errors.length === 0;
    }
}

export default connect (
    state => ({
        alerted: state.displayOptions.alerted,
        error: state.displayOptions.error.user,
        isLoading: state.displayOptions.isLoading.userCreate || state.displayOptions.isLoading.userEdit,
        user: state.user,
        displayModal: state.displayOptions.displayModal,
        modalContent: state.displayOptions.modalContent,
        cities: state.cities.map(element => element.name)
    }),
    dispatch => ({
        addUser: (user) => dispatch(Actions.addUser(user)),
        updateUser: (user) => dispatch(Actions.updateUser(user)),
        closeModal: () => dispatch({ type: 'CLOSE_MODAL' }),
        setUser: (user) => dispatch({ type: 'SET_USER', payload: user })
    })
)(Modal);
