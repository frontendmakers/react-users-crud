import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Datetime from 'react-datetime';
import Moment from 'moment';
require ('moment/locale/ru');

export default class UserEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            user: props.user
        });
    }
    componentWillReceiveProps(nextProps) {
        this.setState( { user: nextProps.user });
    }
    handleChange(id, value) {
        console.log(value);
        this.setState( prevState => ({
            user: {
                ...prevState.user,
                [id]: value
            }
        }));
    }
    render() {
        return (
            <div className="row">
                <h4 className="center-align">Edit User Dialog (Id = {this.state.user.id})</h4>
                <div className="col s10 offset-s1 l9 offset-l1 input-field">
                    <span>Name</span>
                    <input value={ this.state.user.name } 
                        onChange={ (e) => this.handleChange('name', e.target.value) }
                        disabled={this.props.isLoading}
                    />
                </div>
                <div className="col s10 offset-s1 l9 offset-l1">
                    <span>City</span>                   
                    <Select
                        value={ { value:this.state.user.city, label: this.state.user.city } }
                        onChange={ (e) => this.handleChange('city', e ? e.value : '') }
                        options={ this.props.cities.map(element => 
                            { return { value: element, label: element } }) 
                        }
                        placeholder="city"
                        disabled={this.props.isLoading}
                    >
                    </Select>
                </div>
                <div className="col s10 offset-s1 l2 input-field">
                    <label>
                        <input checked={this.state.user.active} 
                        onChange={ (e) => this.handleChange('active', e.target.checked) }
                        type="checkbox"  
                        disabled={this.props.isLoading}
                        />
                        <span>Active</span>
                    </label>
                </div>
                <div className="col s10 offset-s1 input-field">
                    <span>Email</span>
                    <input 
                        value={ this.state.user.email }
                        onChange={ (e) => this.handleChange('email', e.target.value) }
                        type="email" 
                        placeholder="Email" 
                        disabled={this.props.isLoading}
                    />
                </div>
                <div className="col s10 offset-s1 input-field">
                    <span>Last Activity</span>
                    <Datetime
                        locale="ru"
                        utc={true}
                        value={ this.state.user.lastActivity }
                        onChange={ (e) => this.handleChange('lastActivity', e._d) }
                        placeholder="Last Activity"
                        isValidDate={ (currentDate) => 
                            {
                                return currentDate.isBefore(Datetime.moment()) 
                            }
                        }
                    />
                </div>
                <div className="col s10 offset-s1">
                    <div className="row">
                        <div className="col s10 offset-s1 m5">
                            <button className="btn modal-btn" onClick={ () => this.props.updateUser(this.state.user)}>
                                Update
                            </button>
                        </div>
                        <div className="col s10 offset-s1 m5 offset-m2">
                            <button className="btn modal-btn" onClick={this.props.closeModal}>Cancel</button> 
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

