import React from 'react';
import { Link } from 'react-router-dom';
import querystring from 'query-string';
import moment from 'moment';

export default class User extends React.Component {
    render() {
        return (
            <div className={this.getCardStyle()} onClick={ this.props.toggleSelected }>
                <div className="card-content">
                    <p> Id: { this.props.user.id } </p>
                    <p>Name : { this.props.user.name } </p>
                    <p>City : { this.props.user.city } </p>
                    <p>Active : { this.props.user.active.toString() } </p>
                    <p>E-mail : { this.props.user.email }</p>
                    <p>Last Activity : { this.formattedLastActivity(this.props.user.lastActivity) } </p>
                    <div className="row">
                        <div className="col s10 offset-s1 m6">
                            <Link to={ this.getUsersPath() } onClick={ (e) => { e.stopPropagation(); } }>
                                <button className="btn waves-effect card-btn">
                                    Edit
                                </button> 
                            </Link>
                        </div>
                        <div className="col s10 offset-s1 m6 ">
                            <button className="btn waves-effect card-btn" 
                                onClick={(e) => {
                                    e.stopPropagation();
                                    this.props.deleteUser(this.props.user.id)}
                                }
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    getCardStyle() {
        let selected =  this.props.isSelected ? 'teal' : 'teal lighten-2';
        let toDelete = this.props.toDelete ? 'card-to-delete' : '';
        return `card brown-text text-darken-3 ${selected} ${toDelete}`
    } 
    getUsersPath() {
        let search = this.props.location.search;
        let searchObj = querystring.parse(search);
        searchObj.edit = this.props.user.id;
        search = querystring.stringify(searchObj);
        return {
            pathname: 'users',
            search: search
        };
    }
    formattedLastActivity(lastActivity) {
        return moment(lastActivity).isValid() ? moment(lastActivity).format('DD-MM-Y hh:mm') : '-';
    }
}
