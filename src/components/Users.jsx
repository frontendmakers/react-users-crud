import React from 'react';
import User from './User';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import * as ModalActions from '../actions/modal';
import queryString from 'query-string';
import { Pagination } from './Pagination';
import Preloader from './Preloader';
import Alert from './Alert';
import {TransitionMotion, spring} from 'react-motion';

const userForAdd = {
    "name": "Mike",
    "city": "Kazana",
    "active": true,
    "email": "Oleta_Trantow@example.org",
    "lastActivity": "2017-10-06T10:17:14.675Z",
  };

const willEnter = () => ({ opacity: 0 });
const willLeave = () => ({ 
    opacity: spring(0, { stiffness: 140, precision: 0.1 }),
    scale: spring(0)
});

export class Users extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        let [page, search] = [1, {}];
        if (values.page) {
            page = values.page;
            this.props.setCurrentPage(page);
        } else {
            this.props.setCurrentPage(1);
        }
        if (values.q || values.city || values.active) {
            search.q = values.q ? values.q : '';
            search.city = values.city ? [].concat(values.city) : '';
            search.active = values.active ? values.active : null;
        }
        if (values.create === 'true') {
            this.props.createUser();
        } else if (values.edit) {
            this.props.editUser(values.edit, this.props.users);
        }
        this.props.getNumberUsers(search)
        this.props.getUsers(page, search);
    }

    componentWillReceiveProps(nextProps) {
        if (_.isEqual(nextProps.location.search, this.props.location.search)) {
            return;        
        }
        const values = queryString.parse(nextProps.location.search);
        const oldValues = queryString.parse(this.props.location.search);
       
        let [page, search] = [1, {}];
        if (values.page) {
            page = values.page;
            this.props.setCurrentPage(page);
        } else {
            this.props.setCurrentPage(1);
        }
        if (values.create === 'true') {
            this.props.createUser();
            return;
        }
        if (values.edit) {
            this.props.editUser(values.edit, this.props.users);
            return;
        }
        if ( oldValues.page === values.page 
            && oldValues.q === values.q 
            && oldValues.city === values.city 
            && oldValues.active === values.active ) {
            return;        
        }

        if (values.q || values.city || values.active) {
            search.q = values.q ? values.q : '';
            search.city = values.city ? [].concat(values.city) : '';
            search.active = values.active ? values.active : null;
        }


        this.props.getNumberUsers(search)
        this.props.getUsers(page, search);
    }

    render() {
        const usersMap = (
            <TransitionMotion 
                willEnter={ willEnter }
                willLeave={  willLeave }
                styles={ 
                    this.props.isLoading ? 
                    []
                    :
                    this.props.users.map((user, index) => {
                        return { 
                                data: user, 
                                key: index, 
                                style: { 
                                    opacity: spring(1, { stiffness: 140 }),
                                    // scale: spring(1)
                                } 
                            }
                        })
                }
            >
                { users =>
                    <div>
                    { 
                        users.map(user => (
                            <div className="col s10 offset-s1 m6 xl4" key={ user.key }
                                style={
                                    {
                                        opacity: user.style.opacity,
                                        // transform: `scale(${ user.style.scale })` 
                                    }
                                } 
                             >
                                <User
                                    user={ user.data } 
                                    toggleSelected={() => this.props.toggleSelected(user.data.id)}
                                    deleteUser={this.props.deleteUser} 
                                    location={ this.props.location }
                                    isSelected = { this.props.selected.indexOf(user.data.id) !== -1 }
                                    toDelete = { this.props.usersToDelete.indexOf(user.data.id) !== -1 }
                                    >
                                </User> 
                            </div>
                        )
                    )}
                    </div>
                }
            </TransitionMotion>
                
        );

        return (
             <div className="container">
                <div className="alert-container">
                    <Alert alerted={ this.props.alerted }></Alert>
                </div>
                <h1 className="center-align">Users</h1>
                {
                    this.props.error.length > 0 ?
                    <div className="error">
                        { this.props.error }
                    </div>
                    :
                    <div className="row">
                        {
                        this.props.isLoading && 
                        <Preloader></Preloader> 
                        }
                        { usersMap }
                    </div>
                }
                {/* <button onClick={ this.props.getUsers }>Get Users</button> */}
                {/* <button onClick={ () => this.addUser() }>Add User</button> */}
                <div className="pagination-container">
                    { !this.props.isLoading && this.props.error.length === 0 && this.props.numberPages > 1 &&
                        <Pagination 
                        currentPage={ this.props.currentPage } 
                        location={ this.props.location }
                        numberPages={ this.props.numberPages }
                        ></Pagination>
                    }
                </div>
            </div> 
        );
    }
  
    addUser() {
        this.props.addUser( userForAdd);
    }
    
}

export default connect(
    (state) => ({
        alerted: state.displayOptions.alerted,
        error: state.displayOptions.error.users,
        isLoading: state.displayOptions.isLoading.users,
        users: state.users,
        user: state.user,
        currentPage: state.displayOptions.currentPage * 1,
        numberPages: state.displayOptions.numberPages,
        selected: state.displayOptions.selected,
        usersToDelete: state.displayOptions.usersToDelete
    }),
    (dispatch) => ({
        addUser: user => dispatch(Actions.addUser(user)),
        createUser: () => dispatch(ModalActions.showUserCreateModal()),
        editUser: (id, usersInState) => {
            // dispatch(ModalActions.showUserEditModal());
            dispatch(Actions.getUser(id, usersInState));
        },
        getUsers: (page, search) => dispatch(Actions.getUsers(page, search)),
        getNumberUsers: (search) => dispatch(Actions.getNumberUsers(search)),
        deleteUser: (id) => dispatch(Actions.deleteUser(id)),
        setCurrentPage: (page) => dispatch({ type: 'SET_CURRENT_PAGE', payload: page}),
        toggleSelected: (id) => dispatch({ type: 'TOGGLE_CARD_SELECTED_BY_ID', payload: id })
    }) )(Users);
