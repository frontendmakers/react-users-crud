import React from 'react';
import Users from './Users';
import Filters from './Filters';
import Modal from './Modal';

// const Home = () => (
//   <div>
//     <Filters></Filters>
//     <Users></Users>
//     <Modal></Modal>
//   </div>
// )

class Home extends React.Component {
  componentDidMount() {
    console.log(this.props);
  }
  render() {
    return (
      <div>
        <Filters {...this.props}></Filters>
        <Users {...this.props}></Users>
        <Modal {...this.props}></Modal>
      </div>
    );
  }
}

export default Home;