import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Datetime from 'react-datetime';


export default class UserCreate extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            user:{
                name: '',
                city: '',
                active: false,
                email: '',
                lastActivity: ''
            }
        };
        console.log(this.props.cities.map(element => { return { value: element, label: element } }));
    }
    handleChange(id, value) {
        console.log(value);
        this.setState(prevState =>({
            user: {
                ...prevState.user,
                [id]: value
            }
        }));
    }
    render() {
        return(
            <div className="row">
                <h4 className="center-align">Create User Dialog</h4>
                <div className="col s10 offset-s1 input-field">
                    <span>Name</span>
                    <input 
                        value={this.state.user.name}
                        onChange={(e) => this.handleChange('name', e.target.value)}
                        disabled={this.props.isLoading}
                    />
                </div>
                <div className="col s8 offset-s1">
                    <span>City</span>
                    <Select
                        className="modal-select-city"
                        value={ { value:this.state.user.city, label: this.state.user.city } }
                        onChange={ (e) => this.handleChange('city', e ? e.value : '') }
                        options={ this.props.cities.map(element => 
                            { return { value: element, label: element } }) 
                        }
                        disabled={this.props.isLoading}
                    >
                    </Select>
                </div>
                <div className="col s10 offset-s1 m2 input-field">
                    <label>
                        <input type="checkbox"
                            checked={this.state.user.active}
                            onClick={(e) => this.handleChange('active', e.target.checked)}
                            disabled={this.props.isLoading}
                        />
                        <span>Active</span>
                    </label>
                </div>
                <div className="col s10 offset-s1 input-field">
                    <span>Email</span>
                    <input 
                        value={this.state.user.email}
                        onChange={ (e) => this.handleChange( 'email', e.target.value) }
                        disabled={this.props.isLoading}
                    />
                </div>
                <div className="col s10 offset-s1 input-field">
                    <span>Last Activity</span>
                    <Datetime
                        value={ this.state.user.lastActivity }
                        utc={true}
                        onChange={ (e) => this.handleChange('lastActivity', e._d) }
                        isValidDate={ (currentDate, selectedDate) => {
                            return currentDate.isBefore(Datetime.moment());
                        } 
                    }
                    />

                </div>
                <div className="col s10 offset-s1">
                    <div className="row">
                        <div className="col s10 offset-s1 m5">
                            <button className="btn modal-btn" onClick={ () => this.props.addUser(this.state.user) }>Create</button>
                        </div>
                        <div className="col s10 offset-s1 m5 offset-m2">
                            <button className="btn modal-btn" onClick={ this.props.closeModal}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
