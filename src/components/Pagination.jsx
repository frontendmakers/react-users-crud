import React from 'react';
import { Link } from 'react-router-dom';
import querystring from 'query-string';

export class Pagination extends React.Component {
  getUsersPath(dif) {
    let searchObj = querystring.parse(this.props.location.search);
    if (searchObj.page) {
      searchObj.page = searchObj.page * 1 + dif;
    } else {
      searchObj.page = 1 + dif;
    }
    let search = querystring.stringify(searchObj);
    return {
     path: 'users',
     search: search 
    };
  }
  render() {
    const props = this.props;
    return (  
      <ul className="pagination">
        {
          props.currentPage > 1 &&
          <li >
            <Link to={ this.getUsersPath(-1) }>
              <i className="material-icons">chevron_left</i>
            </Link>
          </li> 
        }
        {
          props.currentPage > 2 &&
          <li  className="waves-effect">
            <Link to={ this.getUsersPath(-2) }>
              { props.currentPage - 2 }
            </Link>
          </li>
        }
        {
          props.currentPage > 1 &&
          <li  className="waves-effect">
            <Link to={ this.getUsersPath(-1) }>
              { props.currentPage - 1 }
            </Link>
          </li>
        }
        <li className="active">
          <Link to={ this.getUsersPath(0) }>
            { props.currentPage }
          </Link>
        </li>
        {
          props.currentPage < props.numberPages  &&
          <li className="waves-effect">
            <Link to={ this.getUsersPath(1) }>
              { props.currentPage + 1 }
            </Link>
          </li>
        }
        {
          props.currentPage + 2 <=  props.numberPages &&
          <li  className="waves-effect">
            <Link to={this.getUsersPath(2)}>
              { props.currentPage + 2 }
            </Link>
          </li>
        }
        {
          props.currentPage < props.numberPages &&
          <li  className="waves-effect">
            <Link to={this.getUsersPath(1)}>
              <i className="material-icons">chevron_right</i>
            </Link>
          </li>
        }
      </ul>
    );
  }
}