import React from 'react';
import { TransitionMotion, spring } from 'react-motion';

const willEnter = () => ({ opacity: 0 });
const willLeave = () => ({ opacity: spring(0, { stiffness: 120, precision: 0.1 }) });

export default (props) => (
  <div>
    <TransitionMotion
      willLeave={ willLeave }
      willEnter={ willEnter }
      styles={
        props.alerted.visible  ?
          [{
          key: 'alert',
          data: {},
          style: {
            opacity: spring(1)
          }}]
          :
          []
      }
    >
      {
        items => (
          <div>
            { items.map(item =>
              {
                console.log(item.style.opacity);
                return (
                <div className="alert" style={ item.style } key={ item.key }>
                  { props.alerted.message }
                </div>
                )
              }
              )}
          </div>
       )
    }
    </TransitionMotion>
  </div>
);
