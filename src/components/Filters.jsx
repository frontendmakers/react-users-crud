import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as Actions from '../actions';
import * as ModalActions from '../actions/modal';
import querystring from 'query-string';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

const _ = require('lodash');
export class Filters extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            search: {
                q: '', active: null, city: ''
            }
        };
        this.props.getCities();
        this.debouncedSearch =  _.debounce((id, value) => {
            let searchObj = _.pickBy(this.state.search, value => value || value === false);
            delete searchObj[id];
            console.log('deb');
            let changeSearch = value ? { [id]: value } : {};
            const search = querystring.stringify({ ...searchObj,...changeSearch });
            this.props.history.push(`users?${search}`);

        }, 1000);
    }
    
    handleChange(id, value) {
        console.log({value});
        if (id === 'active' && value === 'null') {
            value = null;
        }
        this.setState( { search: {
            ...this.state.search,
            [id]: value
            }
        });
        console.log(this.state);
        this.debouncedSearch(id, value);
    }

    getUsersPath() {
        const searchObj = querystring.parse(this.props.location.search);
        searchObj.create = true;
        const search = querystring.stringify(searchObj);
        return {
            pathname: "users",
            search: search
        }
    }
    
    addUser() {
        this.props.setDisplayModal(true);
        this.props.setModalContent({userCreate: true});
    }

    selectedCities() {
        return this.state.city.map(element => { return {value: element, label: element } });
    } 
    
    render() {
        let searchObj = _.pickBy(this.state.search, value => value || value === false);
        let search = querystring.stringify(searchObj);

        return (
            <div className="filters">
                <div className="container row">
                    <div className="row">
                        <div className="col s12 l4 input-field">
                            <input type="text" 
                                value={ this.state.search.q }  
                                onChange={(e) => this.handleChange('q', e.target.value)}
                                placeholder="Search"/>
                        </div>
                        <div className="col s12 l2 input-field left-align">
                            <p>
                                <label>
                                    <input name="active" 
                                        type="radio"
                                        value="null"
                                        onChange={(e) => this.handleChange('active', e.target.value)}
                                    />
                                    <span>All</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input name="active" 
                                    type="radio" 
                                    value={true}
                                    onChange={(e) => this.handleChange('active', e.target.value)}
                                    />
                                    <span>Active</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input name="active"
                                    type="radio" 
                                    value={false}
                                    onChange={(e) => this.handleChange('active', e.target.value)}
                                    />
                                    <span>Inactive</span>
                                </label>
                            </p>
                        </div>
                        <div className="col s12 l6">
                            <Select
                                value={this.state.search.city}
                                multi
                                onChange={(e) => this.handleChange('city', e.map(element => element.value))}
                                options={  this.props.cities }
                                placeholder="Cities"
                            ></Select>
                            <label>Cities</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12 m4">
                            <Link to={{ pathname: "users", search: search }}>
                                <button className="btn filters-btn">
                                    Search
                                </button>
                            </Link>
                        </div>
                        <div className="col s12 m4">
                            <Link to={this.getUsersPath()}>
                                <button className="btn filters-btn" onClick={() => this.addUser()}>Add User</button>
                            </Link>
                        </div>
                        <div className="col s12 m4">
                            <button className="btn filters-btn"
                                onClick={() => this.props.deleteSelected(this.props.selected)}   
                            >
                                Delete Selected
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
} 

export default connect(
    (state) => ({
        selected: state.displayOptions.selected,
        cities: state.cities.map(element => { return { value: element.name, label: element.name } })
    }),
    (dispatch) => ({
        getCities: () => dispatch(Actions.getCities()),
        searchUsers: (search) => dispatch(Actions.getUsers(1, search)),
        showUserCreateModal: () => dispatch(ModalActions.showUserCreateModal()),
        setDisplayModal: (displayModal) => dispatch({type: 'SET_DISPLAY_MODAL', payload: displayModal}),
        setModalContent: (modalContent) => dispatch({type: 'SET_MODAL_CONTENT', payload: modalContent}),
        deleteSelected: (selected) => dispatch(Actions.deleteUsers(selected))
    })
)(Filters);

