import 'whatwg-fetch';

const _ = require('lodash');
const DEFAULT_OPTIONS = {
    headers: {
        'Content-Type': 'application/json'
    }
};
const BASE_URL = 'http://localhost:3000/';

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response.json();
    } else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
};

function request(url, options) {
    return new Promise(function(resolve, reject) {
        console.log(url);
        fetch(url, options)
            .then(checkStatus)
            .then(resolve)
            .catch(reject);
    });
}

// export function getUsers(page, limit, onSuccess) {
//     return request(BASE_URL + 'users/?_page=' + page + '&_limit=' + limit, {}, onSuccess);
// }

export function getUrlSuffixFromSearch(search) {
    if (!search) return '';
    let q = search.q ? '&q=' + search.q : '';
    let active = search.active ? '&active=' + search.active : '';
    console.log('City=',search.city);
    let city = search.city && search.city.length ? '&city='.concat(search.city.join('&city=')) : '';
    // let city = search.city && search.city.length ? '&city='.concat(search.city) : '';
    return ''.concat(q, active, city);
}

export function searchUsers(page, limit, search) {
    let urlSuf = getUrlSuffixFromSearch(search);
    console.log(urlSuf);
    return request(BASE_URL + 'users?_page=' + page + '&_limit=' + limit + '&_sort=id&_order=desc' + urlSuf, {});
}

export function getUser(id) {
    return request(BASE_URL + 'users/' + id, {});
}

export function getNumberUsers(search) {
    console.log(search);
    let urlSuf = getUrlSuffixFromSearch(search);
    urlSuf = urlSuf.slice(1);
    console.log(urlSuf);
    return request(BASE_URL + 'users?' + urlSuf, {});
}

export function getCities() {
    return request(BASE_URL + 'cities');
}

export function addUser(user) {
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    };
    return request(BASE_URL + 'users/', options);
}

export function deleteUser(id) {
    let options = {
        method: 'DELETE'
    };
    return request(BASE_URL + 'users/' + id, options);
}

// export function deleteUsers(idArray, onSuccess) {
//     let options = {
//         method: 'DELETE'
//     };
//     let urlSuf = '?id='.concat(idArray.join('&id='));
//     return request(BASE_URL + 'users' + urlSuf, options, onSuccess);
// }

export function updateUser(user, onSuccess) {
    let options = _.merge(DEFAULT_OPTIONS, {
        method: 'PUT',
        body: JSON.stringify(user)
    });

    return request(BASE_URL + 'users/' + user.id, options, onSuccess);
}
