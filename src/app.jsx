import './scss/style.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Home from './components/Home';
import { store } from './store';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { hot } from 'react-hot-loader'

const renderApplication = () => {
  ReactDOM.render(
    <Provider store = { store }>
        <Router>
          <div>
            <Route path="/" component={Home}/>
            <Route path="users" component={Home}/>
          </div>
        </Router>
    </Provider>,
    document.querySelector('#root')
  );
}

renderApplication(Home);
export default hot(module)(Home);


if (module.hot) {
  module.hot.accept("./components/Home", () => {
    renderApplication();
  });
}
