import { combineReducers } from 'redux';

import users from './users';
import user from './user';
import displayOptions from './displayOptions';
import cities from './cities';

export default combineReducers({
  users,
  user,
  cities,
  displayOptions
});