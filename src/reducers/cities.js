
export default function cities(state = [], action) {
  if (action.type === 'SET_CITIES') {
    state = [ ...action.payload ];
  }
  return state;
}