const initialState = 
{
  "id": 3,
  "name": "Sammie Rutherford",
  "city": "West Meaganshire",
  "active": true,
  "email": "Isaac92@example.com",
  "lastActivity": "2017-10-04T07:24:01.776Z"
}

export default function user(state = {}, action) {
  if (action.type === 'SET_USER') {
    return {...state, ...action.payload};
  } else if (action.type === 'CLEAR_USER') {
    return { ...state, ...{ id: '', name: '', city: '', active: undefined, email: '', lastActivity: '' }};
  }
  return state;
}