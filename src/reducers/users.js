const initialState = [
  {
    "id": 2,
    "name": "Janet Effertz",
    "city": "South Anabelton",
    "active": true,
    "email": "Heaven.Predovic74@example.com",
    "lastActivity": "2017-10-20T05:15:52.720Z"
  },
  {
    "id": 3,
    "name": "Sammie Rutherford",
    "city": "West Meaganshire",
    "active": true,
    "email": "Isaac92@example.com",
    "lastActivity": "2017-10-04T07:24:01.776Z"
  }
];

export default function users(state = [], action) {
  switch (action.type) {
    case 'ADD_USER': {
      return [action.payload, ...state.slice(0, -1)];
      break;
    }
    case 'DELETE_USER': {
      let users = state.filter(user => user.id !== action.id)
      return users;
    }
    case 'SET_USERS': {
      return [...action.payload];
    }
    case 'SET_USER_TO_USERS': {
      let users = state.map(value => action.payload.id === value.id ? action.payload : value); 
      return users;
    }
  }
  return state;
}