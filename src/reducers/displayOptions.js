
const initialState = {
  displayModal: false,
  isLoading: {
      userCreate: false,
      userEdit: false,
      users: false
  },
  modalContent: {
      userCreate: false,
      userEdit: false
  },
  errorsForm: [],
  error: {
      users: '',
      user: ''
  },
  numberUsers: 0,
  numberPages: 1,
  currentPage: 1,
  selected: [],
  usersToDelete: [],
  alerted: {
      visible: false,
      message: ''
  }
};
export default function displayOptions(state = initialState, action) {
  switch (action.type) {
    case 'SET_DISPLAY_MODAL': {
      return {...state,  displayModal: action.payload }; 
    }
    case 'SET_MODAL_CONTENT': {
        return { ...state, modalContent: {...state.modalContent, ...action.payload}};
    }
    case 'CLEAR_MODAL_CONTENT': {
        return { ...state, modalContent: {...state.modalContent, ...initialState.modalContent}};
    }

    case 'SET_IS_LOADING': {
        return { ...state, isLoading: {...state.isLoading, ...action.payload}};
    }

    case 'SET_ERRORS_FORM': {
      return {...state,  errorsForm: action.payload }; 
    }
    case 'CLEAR_ERRORS_FORM': {
      return {...state,  errorsForm: [] }; 
    }

    case 'SET_ERROR': {
        return {...state,  error: { ...state.error, ...action.payload }}; 
      }
    case 'CLEAR_ERROR': {
        return {...state,  error: { ...initialState.error } }; 
    }
    case 'CLEAR_ERROR_USERS': {
        return {...state,  error: {...state.error,  users: initialState.error.users } }; 
    }
    case 'CLEAR_ERROR_USER': {
        return {...state,  error: {...state.error,  user: initialState.error.user } }; 
    }

    case 'SET_NUMBER_USERS': {
        return {...state,  numberUsers: action.payload }; 
    }

    case 'INC_NUMBER_USERS': {
        let newNumberUsers = state.numberUsers + 1;
        let newNumberPages = Math.ceil(newNumberUsers / action.payload);
        return { ...state, numberUsers: newNumberUsers, numberPages: newNumberPages };
    }
    case 'DEC_NUMBER_USERS': {
        let newNumberUsers = state.numberUsers - 1;
        let newNumberPages = Math.ceil(newNumberUsers / action.payload);
        return { ...state, numberUsers: state.numberUsers - 1, numberPages: newNumberPages };
    }

    case 'SET_NUMBER_PAGES': {
        return {...state,  numberPages: action.payload }; 
    }
    case 'SET_CURRENT_PAGE': {
        return {...state,  currentPage: action.payload }; 
    }

    case 'TOGGLE_CARD_SELECTED_BY_ID': {
        console.log(action.payload);
        let length = state.selected.length;
        let selected = state.selected.filter(value => value != action.payload);

        if (selected.length === length) {
            selected = [].concat(selected, action.payload);
        }
        return { ...state, selected: selected };
    }

    case 'CLEAR_SELECTED': {
        return { ...state, selected: [] };
    }

    case 'CLOSE_MODAL': {
        return { 
            ...state,  
            displayModal: false, 
            modalContent: {...initialState.modalContent},
            errorsForm: []
        };
    }

    case 'SET_ALERTED': {
        return {
            ...state,
            alerted:{
                ...state.alerted,
                ...action.payload
            }
        };
    }

    case 'SET_USERS_TO_DELETE': {
        return {
            ...state,
            usersToDelete: action.payload
        };
    }

    case 'CLEAR_USERS_TO_DELETE': {
        return { 
            ...state,
            usersToDelete: initialState.usersToDelete
        };
    }

    case 'PUSH_TO_USERS_TO_DELETE': {
        return {
            ...state,
            usersToDelete: [
                ...state.usersToDelete, action.payload
            ]
        };
    }

    case 'DELETE_FROM_USERS_TO_DELETE': {
        return {
            ...state,
            usersToDelete: state.usersToDelete.filter(element => element !== action.payload)
        };
    }

  }
  return state;
}